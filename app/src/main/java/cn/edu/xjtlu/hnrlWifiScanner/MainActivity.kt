package cn.edu.xjtlu.hnrlWifiScanner

//import android.icu.text.SimpleDateFormat
import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import android.net.wifi.ScanResult
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.lifecycleScope
import com.google.android.material.textfield.TextInputEditText
import kotlinx.coroutines.launch
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.time.*
import java.util.*

class MainActivity : AppCompatActivity() {
    // UI elements
    private lateinit var tietLocBuilding: TextInputEditText
    private lateinit var tietTextLocFloor: TextInputEditText
    private lateinit var tietLocRoom: TextInputEditText
    private lateinit var tietTextLocX: TextInputEditText
    private lateinit var tietLocY: TextInputEditText
    private lateinit var tietLocZ: TextInputEditText
    private lateinit var tvScanResults: TextView
    private lateinit var tvSrNumber: TextView
    private lateinit var tvFpNumber: TextView

    // Wi-Fi scanning
    private lateinit var wifiManager: WifiManager
    private lateinit var wifiReceiver: WifiBroadcastReceiver
    private lateinit var fingerprintDateAndTime: String
    private lateinit var scanResults: List<ScanResult>

    // fingerprint DB
    private var srNumber: Int = 0 // number of saved AP scan results
    private var fpNumber: Int = 0  // number of saved fingerprints
    private var fpSaved: Boolean = true
    private lateinit var fingerprint: MutableList<ApScan>
    private lateinit var fingerprintDatabase: FingerprintDatabase
    private val fingerprintViewModel: FingerprintViewModel by viewModels {
        FingerprintViewModelFactory((application as FingerprintApplication).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        wifiManager = this.applicationContext.getSystemService(WIFI_SERVICE) as WifiManager

        // register a broadcast receiver
        wifiReceiver = WifiBroadcastReceiver()
        registerReceiver(wifiReceiver, IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION))

        val buttonScan = findViewById<Button>(R.id.btn_scan)
        val buttonSave = findViewById<Button>(R.id.btn_save)
        val buttonExport = findViewById<Button>(R.id.btn_export)
        val buttonClear = findViewById<Button>(R.id.btn_clear)
        tietLocBuilding = findViewById(R.id.tiet_loc_building)
        tietTextLocFloor = findViewById(R.id.tiet_loc_floor)
        tietLocRoom = findViewById(R.id.tiet_loc_room)
        tietTextLocX = findViewById(R.id.tiet_loc_x)
        tietLocY = findViewById(R.id.tiet_loc_y)
        tietLocZ = findViewById(R.id.tiet_loc_z)
        tvScanResults = findViewById(R.id.tv_scan_results)
        tvSrNumber = findViewById(R.id.tv_sr_number)
        tvFpNumber = findViewById(R.id.tv_fp_number)
        buttonScan.setOnClickListener { askAndStartScanWifi() }
        buttonSave.setOnClickListener { saveFingerprint() }
        buttonExport.setOnClickListener { exportFingerprintDatabase() }
        buttonClear.setOnClickListener { clearFingerprintDatabase() }

        // fingerprint DB
        fingerprintDatabase = (application as FingerprintApplication).database

        // observer for all the fingerprints in the DB
        fingerprintViewModel.allFingerprints.observe(this) { fingerprints ->
            if (fingerprints.isNullOrEmpty()) {
                srNumber = 0
                fpNumber = 0
            } else {
                srNumber = fingerprints.size
                fpNumber = fingerprints.last().fingerprintId + 1
            }
            tvSrNumber.text = String.format("%04d", srNumber)
            tvFpNumber.text = String.format("%03d", fpNumber)
        }
    }

    private fun askAndStartScanWifi() {
        // ask for user permission for Android Level >= 23.
        if (checkCoarseLocationPermission()) {
            Log.d(LOG_TAG, "Requesting Wi-Fi scan permissions")

            // request permissions
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_WIFI_STATE,
                    Manifest.permission.ACCESS_NETWORK_STATE
                ),
                WIFI_REQUEST_CODE
            )
            return
        }
        Log.d(LOG_TAG, "Permissions already granted")
        doStartScanWifi()
    }

    private fun checkCoarseLocationPermission(): Boolean {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
    }

    private fun saveFingerprint() {
        if (fpSaved) {
            toast("The fingerprint already saved.")
        } else {
            toast("Saving a fingerprint ...")
            fingerprintViewModel.insert(fingerprint.toList())
            this.fpSaved = true
        }
    }

    private fun exportFingerprintDatabase() {
        if (!checkExternalStoragePermission()) {
            Log.d(LOG_TAG, "onCreate: Permission was not granted, request")
            requestExternalStoragePermission()
        } else {
            Log.d(LOG_TAG, "onCreate: Permission already granted, export DB")
            toast("Exporting the fingerprint database ...")

            // checkpoint the DB before exporting it
            lifecycleScope.launch { // based on the suggestion of "https://stackoverflow.com/a/48265349"
                fingerprintViewModel.checkpoint()
            }

            lateinit var exportDB: File
            val folder = File(
                Environment.getExternalStorageDirectory().toString() + File.separator + "hnrlWifiScanner"
            )
            if (folder.isDirectory || folder.mkdir()) {
                val originalDB = fingerprintDatabase.openHelper.writableDatabase?.path
                exportDB = File(folder, "wifi_fingerprint_database.db")

                // copy the DB to external storage
                try {
                    File(originalDB.toString()).copyTo(exportDB, overwrite = true)
                } catch (e: IOException) {
                    Log.e(LOG_TAG, e.message.toString())
                    toast("Exporting the fingerprint database failed  ...")
                }
                toast("The database exported to external storage space: $exportDB")

                // email the exported DB
                sendEmail(exportDB)
            } else {
                toast("Exporting the fingerprint database failed  ...")
            }
        }
    }

    private fun sendEmail(attachment: File) {
        try {
//            val recipient = "email@address.here"
            val subject = "HNRL Wi-Fi Scanner Fingerprint DB"
            val message = "Automatically-generate email by HNRL Wi-Fi Scanner App."
            val emailIntent = Intent(Intent.ACTION_SEND).apply {
                type = "text/plain"
//                putExtra(Intent.EXTRA_EMAIL, arrayOf(recipient))
                putExtra(Intent.EXTRA_SUBJECT, subject)
                putExtra(Intent.EXTRA_TEXT, message)
                putExtra(
                    Intent.EXTRA_STREAM,
                    FileProvider.getUriForFile(
                        applicationContext,
                        "$packageName.fileProvider",
                        attachment
                    )
                )
            }
            this.startActivity(Intent.createChooser(emailIntent, "Sending email ..."))
        }
        catch (t: Throwable) {
            toast("Sending email failed: $t")
        }
    }

    private fun checkExternalStoragePermission(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            // Android 11 (R) or above
            Environment.isExternalStorageManager()
        } else {
            val write = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            val read = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED
        }
    }

    private fun requestExternalStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            // Android 11 (R) or above
            try {
                Log.d(LOG_TAG, "requestPermission: try")
                val intent = Intent()
                intent.action = Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION
                val uri = Uri.fromParts("package", this.packageName, null)
                intent.data = uri
                storageActivityResultLauncher.launch(intent)
            }
            catch (e: Exception) {
                Log.e(LOG_TAG, "requestPermission: ", e)
                val intent = Intent()
                intent.action = Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION
                storageActivityResultLauncher.launch(intent)
            }
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ),
                STORAGE_REQUEST_CODE
                )
        }
    }

    private val storageActivityResultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        Log.d(LOG_TAG, "storageActivityResultLauncher: ")

        // handle the result of the intent
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            // Android 11 (R) or above
            if (Environment.isExternalStorageManager()) {
                Log.d(LOG_TAG, "storageActivityResultLauncher: Manage External Storage Permission is granted")
                exportFingerprintDatabase()
            } else {
                Log.d(LOG_TAG, "storageActivityResultLauncher: Manage External Storage Permission is denied ...")
                toast("Manage External Storage Permission is denied ...")
            }
        } else {
            // TODO
        }
    }

    private fun clearFingerprintDatabase() {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.apply {
            setTitle("Clear the fingerprint DB?")
            setPositiveButton("Clear") { _, _ ->
                fingerprintViewModel.deleteAll()
                }
            setNegativeButton("Cancel") { _, _ ->
                }
        }.create().show()
    }

    private fun doStartScanWifi() {
        wifiManager.startScan()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        Log.d(LOG_TAG, "onRequestPermissionsResult")

        if (requestCode == STORAGE_REQUEST_CODE) {
            if (grantResults.isNotEmpty()) {
                val write = grantResults[0] == PackageManager.PERMISSION_GRANTED
                val read = grantResults[1] == PackageManager.PERMISSION_GRANTED
                if (write && read) {
                    Log.d(LOG_TAG, "onRequestPermissionsResult: External Storage Permission granted")
                    exportFingerprintDatabase()
                } else {
                    Log.d(LOG_TAG, "onRequestPermissionsResult: External Storage Permission denied ...")
                    toast("External Storage Permission denied ...")
                }
            }

        } else {
            when (requestCode) {
                WIFI_REQUEST_CODE -> {
                    // if request is cancelled, the result arrays are empty.
                    if (grantResults.isNotEmpty() && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                        Log.d(LOG_TAG, "Permission granted: " + permissions[0])
                        doStartScanWifi()
                    } else {
                        Log.d(LOG_TAG, "Permission denied: " + permissions[0])
                    }
                }
            }
        }
    }

    override fun onStop() {
        unregisterReceiver(wifiReceiver)
        super.onStop()
    }

    // define class to listen to broadcasts
    internal inner class WifiBroadcastReceiver : BroadcastReceiver() {
        @SuppressLint("SimpleDateFormat")
        override fun onReceive(context: Context, intent: Intent) {
            Log.d(LOG_TAG, "onReceive()")
            toast("Scan complete!")
            val ok = intent.getBooleanExtra(WifiManager.EXTRA_RESULTS_UPDATED, false)
            if (ok) {
                Log.d(LOG_TAG, "Scan OK")
                val sdf = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX")
                } else {
                    // API level 23 ('Z' for UTC)
                    SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                }
                sdf.timeZone = TimeZone.getTimeZone("UTC")
                fingerprintDateAndTime = sdf.format(Date())
                scanResults = if (ActivityCompat.checkSelfPermission(this@MainActivity.applicationContext,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                ) {
                    /*
                    TODO: Consider calling
                    ActivityCompat#requestPermissions
                    here to request the missing permissions, and then overriding
                    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    int[] grantResults)
                    to handle the case where the user grants the permission. See the documentation
                    for ActivityCompat#requestPermissions for more details.
                    */
                    toast("Check the status of Wi-Fi.")
                    return
                } else {
                    wifiManager.scanResults
                }
                showNetworksDetails(fingerprintDateAndTime, scanResults)
            } else {
                Log.d(LOG_TAG, "Scan not OK")
            }
        }
    }

    private fun showNetworksDetails(fingerprintDateAndTime: String, scanResults: List<ScanResult>) {
        tvScanResults.movementMethod = ScrollingMovementMethod()
        tvScanResults.text = ""
        val sb = StringBuilder()
        sb.append("Fingerprint Date and Time: ").append(fingerprintDateAndTime)
        sb.append("\nNumber of APs detected: ").append(scanResults.size)
        fingerprint = mutableListOf()
        for (i in scanResults.indices) {
            val result = scanResults[i]
            sb.append("\n")
            sb.append("\nBSSID: ").append(result.BSSID)
            sb.append("\nSSID: ").append(result.SSID) // Network Name.
            sb.append("\nCapabilities: ").append(result.capabilities)
            if (result.channelWidth == ScanResult.CHANNEL_WIDTH_80MHZ_PLUS_MHZ) {
                sb.append("\nCenter Frequency of 1st Segment [MHz]: ").append(result.centerFreq0)
                sb.append("\nCenter Frequency of 2nd Segment [MHz]: ").append(result.centerFreq1)
            } else {
                sb.append("\nCenter Frequency [MHz]: ").append(result.centerFreq0)
            }
            val channelWidth = when (result.channelWidth) {
                ScanResult.CHANNEL_WIDTH_20MHZ -> "20"
                ScanResult.CHANNEL_WIDTH_40MHZ -> "40"
                ScanResult.CHANNEL_WIDTH_80MHZ -> "80"
                ScanResult.CHANNEL_WIDTH_160MHZ -> "160"
                ScanResult.CHANNEL_WIDTH_80MHZ_PLUS_MHZ -> "160 (80+80)"
                else -> if (Build.VERSION.SDK_INT >= 33) "320" else "" // TBD: This is a temporary solution.
            }
            sb.append("\nChannel Width [MHz]: ").append(channelWidth)
            sb.append("\nFrequency [MHz]: ").append(result.frequency)
            sb.append("\nLevel [dBm]: ").append(result.level)
//                sb.append("\n result.operatorFriendlyName: ").append(result.operatorFriendlyName)
            sb.append("\nTimestamp [ms]: ").append(result.timestamp)
//                sb.append("\n result.venueName: ").append(result.venueName)

            // create room DB records
            fingerprint.add(
                ApScan(
                    id = srNumber + i + 1, // sqlite3 DB row number starts from 1
                    fingerprintId = fpNumber,
                    fingerprintDateTime = fingerprintDateAndTime,
                    device = Build.MANUFACTURER + " " + Build.MODEL,
                    sdkVersion = Build.VERSION.SDK_INT,
                    locBuilding = tietLocBuilding.text.toString(),
                    locFloor = tietTextLocFloor.text.toString().toIntOrNull(),
                    locRoom = tietLocRoom.text.toString(),
                    locCoordinates = "", // TODO: implement UI for it
                    locX = tietTextLocX.text.toString().toFloatOrNull(),
                    locY = tietLocY.text.toString().toFloatOrNull(),
                    locZ = tietLocZ.text.toString().toFloatOrNull(),
                    bssid = result.BSSID,
                    ssid = result.SSID,
                    capabilities = result.capabilities,
                    centerFreq0 = result.centerFreq0,
                    centerFreq1 = result.centerFreq1,
                    channelWidth = channelWidth,
                    frequency = result.frequency,
                    level =  result.level,
                    timestamp = result.timestamp
                )
            )
        }
        tvScanResults.text = sb.toString()
        fpSaved = false
    }

    private fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    companion object {
        private const val LOG_TAG = "HNRL Wi-Fi Scanner"

        // PERMISSION request constants; assign any values
        private const val STORAGE_REQUEST_CODE = 100
        private const val WIFI_REQUEST_CODE = 123
    }
}