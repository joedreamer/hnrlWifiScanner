package cn.edu.xjtlu.hnrlWifiScanner

import androidx.annotation.WorkerThread
import androidx.sqlite.db.SimpleSQLiteQuery
import kotlinx.coroutines.flow.Flow

class FingerprintRepository(private val apScanDao: ApScanDao) {

    // TODO: Replace the following with more meaningful one
    val allFingerprints: Flow<List<ApScan>> = apScanDao.getAll()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(fingerprint: List<ApScan>) {
        apScanDao.insert(fingerprint)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun deleteAll() {
        apScanDao.deleteAll()
    }

//    @Suppress("RedundantSuspendModifier")
//    @WorkerThread
    suspend fun checkpoint() {
        apScanDao.checkpoint(SimpleSQLiteQuery ("pragma wal_checkpoint(full)"))
    }
}