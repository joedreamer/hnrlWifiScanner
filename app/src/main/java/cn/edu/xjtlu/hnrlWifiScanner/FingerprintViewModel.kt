package cn.edu.xjtlu.hnrlWifiScanner

import androidx.lifecycle.*
import kotlinx.coroutines.launch

class FingerprintViewModel(private val repository: FingerprintRepository) : ViewModel() {

    val allFingerprints: LiveData<List<ApScan>> = repository.allFingerprints.asLiveData()

    fun insert(fingerprint: List<ApScan>) = viewModelScope.launch {
        repository.insert(fingerprint)
    }

    fun deleteAll() = viewModelScope.launch {
        repository.deleteAll()
    }

//    fun checkpoint() = viewModelScope.launch {
//        repository.checkpoint()
//    }
    suspend fun checkpoint() = repository.checkpoint()
}

class FingerprintViewModelFactory(private val repository: FingerprintRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FingerprintViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return FingerprintViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}