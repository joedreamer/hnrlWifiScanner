package cn.edu.xjtlu.hnrlWifiScanner

import android.content.Context
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.sqlite.db.SupportSQLiteQuery
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch


// Room DB for fingerprints based on the "entity–attribute–value model"
// (https://en.wikipedia.org/wiki/Entity%E2%80%93attribute%E2%80%93value_model).
// - A fingerprint is a collection of scan results from the WifiManager.
// - ApScan (a record) is based on each element of a ScanResults list (i.e., ScanResult).
//   (i.e., a fingerprint) augmented with the following information common
//   to a fingerprint:
//   - fingerprint_id: a key for queries for further processing
//   - fingerprint_date_time
//   - device (i.e., brand + model)
//   - sdk_version (i.e., Build.VERSION.SDK_INT)

@Entity(tableName = "fingerprint_table")
data class ApScan(
    // fields common to a fingerprint (i.e., a measurement)
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "fingerprint_id") val fingerprintId: Int,
    @ColumnInfo(name = "fingerprint_date_time") val fingerprintDateTime: String?,
    @ColumnInfo(name = "device") val device: String?,
    @ColumnInfo(name = "sdk_version") val sdkVersion: Int?,
    @ColumnInfo(name = "loc_building") val locBuilding: String?,
    @ColumnInfo(name = "loc_floor") val locFloor: Int?,
    @ColumnInfo(name = "loc_room") val locRoom: String?,
    @ColumnInfo(name = "loc_coordinates") val locCoordinates: String?, // e.g., UTM coordinates "17T 630084 4833438"
    @ColumnInfo(name = "loc_x") val locX: Float?,
    @ColumnInfo(name = "loc_y") val locY: Float?,
    @ColumnInfo(name = "loc_z") val locZ: Float?,

    // fields specific to an AP
    @ColumnInfo(name = "bssid") val bssid: String?,
    @ColumnInfo(name = "ssid") val ssid: String?,
    @ColumnInfo(name = "capabilities") val capabilities: String?,
    @ColumnInfo(name = "center_freq0") val centerFreq0: Int?,
    @ColumnInfo(name = "center_freq1") val centerFreq1: Int?,
    @ColumnInfo(name = "channel_width") val channelWidth: String?,
    @ColumnInfo(name = "frequency") val frequency: Int?,
    @ColumnInfo(name = "level") val level: Int?,
    @ColumnInfo(name = "timestamp") val timestamp: Long?
)

@Dao
interface ApScanDao {
//    @Query("SELECT * From fingerprint_table")
//    fun getAll(): List<Fingerprint>

    @Query("SELECT * FROM fingerprint_table ORDER BY id ASC")
    fun getAll(): Flow<List<ApScan>>

    @Query("SELECT * FROM fingerprint_table WHERE fingerprint_id LIKE :fingerprintId")
    fun findByFingerprintId(fingerprintId: Int): Flow<List<ApScan>>

//    @Insert(onConflict = OnConflictStrategy.IGNORE)
//    suspend fun insert(apScan: ApScan)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(fingerprint: List<ApScan>)

//    @Delete
//    suspend fun delete(fingerprint: Fingerprint)

    @Query("DELETE FROM fingerprint_table")
    suspend fun deleteAll()

    @RawQuery
    suspend fun checkpoint(supportSQLiteQuery: SupportSQLiteQuery): Int
}

@Database(entities=[ApScan::class], version=1)
abstract class FingerprintDatabase : RoomDatabase() {

    abstract fun apScanDao(): ApScanDao

    // singleton implementation
    companion object {
        @Volatile
        private var INSTANCE: FingerprintDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): FingerprintDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    FingerprintDatabase::class.java,
                    "wifi_fingerprint_database"
                )
                    .fallbackToDestructiveMigration()
                    .addCallback(FingerprintDatabaseCallback(scope))
//                    .setJournalMode(RoomDatabase.JournalMode.TRUNCATE) // TODO: a temporary solution for export DB
//                    .allowMainThreadQueries() // TODO: a temporary solution to avoid crash resulting from accessing database on the main thread (i.e., calling checkout())
                    .build()
                INSTANCE = instance
                instance // return instance
            }
        }

        private class FingerprintDatabaseCallback(
            private val scope: CoroutineScope
        ) : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                INSTANCE?.let { database ->
                    scope.launch(Dispatchers.IO) {
                        populateDatabase(database.apScanDao())
                    }
                }
            }
        }

        suspend fun populateDatabase(apScanDao: ApScanDao) {
            apScanDao.deleteAll()

            // TODO: Adding any initial fingerprints?
        }

        // close database
        public fun destroyInstance() {
            if (INSTANCE!!.isOpen) INSTANCE!!.close()
            INSTANCE = null
        }
    }
}
