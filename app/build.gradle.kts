plugins {
    id("com.android.application")
//    id("org.jetbrains.kotlin.android")
    kotlin("android")
//    id("com.google.devtools.ksp") version "1.7.20-1.0.7"
    kotlin("android.extensions")
    kotlin("kapt")
}

android {
    namespace = "cn.edu.xjtlu.hnrlWifiScanner"
    compileSdk = 33

    defaultConfig {
        applicationId = "cn.edu.xjtlu.hnrlWifiScanner"
        minSdk = 23
        targetSdk = 33
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        kapt {
            arguments {
                arg("room.schemaLocation", "$projectDir/schemas")
            }
        }
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        sourceCompatibility(JavaVersion.VERSION_1_8)
        targetCompatibility(JavaVersion.VERSION_1_8)
    }

    packagingOptions {
        resources {
            excludes += "META-INF/atomicfu.kotlin_module"
        }
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    implementation("com.google.android.material:material:1.7.0")
    implementation("androidx.core:core-ktx:1.9.0")
    implementation("androidx.appcompat:appcompat:1.5.1")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    implementation("androidx.fragment:fragment-ktx:1.5.5")
    implementation("androidx.room:room-common:2.4.3")
    testImplementation("unit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.4")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.0")

    // to use Room database
//    val roomVersion = "2.4.3"
    implementation("androidx.room:room-runtime:2.4.3")
    annotationProcessor("androidx.room:room-compiler:2.4.3")
    kapt("androidx.room:room-compiler:2.4.3")
//    ksp("androidx.room:room-compiler:$roomVersion")
    implementation("androidx.room:room-ktx:2.4.3")
//    implementation("androidx.room:room-rxjava2:$roomVersion")
//    implementation("androidx.room:room-rxjava3:$roomVersion")
//    implementation("androidx.room:room-guava:$roomVersion")
//    testImplementation("androidx.room:room-testing:$roomVersion")
//    implementation("androidx.room:room-paging:$roomVersion")

    // Lifecycle components
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.5.1")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.5.1")
    implementation("androidx.lifecycle:lifecycle-common-java8:2.5.1")

    // Kotlin components
//    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.8.0") // causing duplicate class errors
    api("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")
    api("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.6.4")
}
