# hnrlWifiScanner
Wi-Fi Scanner for Android mobile devices.
- Scans and stores Wi-Fi fingerprints (including RSSI) in a local Android Room (SQLite) DB.
    - The DB for fingerprints is structured based on the ["entity–attribute–value model"](https://en.wikipedia.org/wiki/Entity%E2%80%93attribute%E2%80%93value_model). 
- Exports the fingerprint DB.
    - Copies the fingerprint DB to the external shared storage (i.e., "hnrlWifiScanner/wifi_fingerprint_database.db") for access from PC via USB.
    - Emails the DB as attachment.
